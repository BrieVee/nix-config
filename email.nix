{ config, pkgs, ... }: {
  imports = [
    (builtins.fetchTarball {
      url =
        "https://gitlab.com/simple-nixos-mailserver/nixos-mailserver/-/archive/nixos-23.05/nixos-mailserver-nixos-23.05.tar.gz";

      # from  set release "nixos-23.05"; nix-prefetch-url "https://gitlab.com/simple-nixos-mailserver/nixos-mailserver/-/archive/$release/nixos-mailserver-$release.tar.gz" --unpack
      sha256 = "1ngil2shzkf61qxiqw11awyl81cr7ks2kv3r3k243zz7v2xakm5c";
    })
  ];

  mailserver = {
    enable = true;
    fqdn = "mail.brie.gay";
    domains = [ "brie.gay" ];

    # Login accounts, password hashes from
    # nix-shell -p mkpasswd --run 'mkpasswd -sm bcrypt'
    loginAccounts = {
      "brie@brie.gay" = {
        hashedPasswordFile = "/etc/email/passwords/brie";
        aliases = [ "admin@brie.gay" "@brie.gay" ];
      };
      "sprite@brie.gay" = {
        hashedPasswordFile = "/etc/email/passwords/sprite";
      };
    };

    certificateScheme = "acme-nginx";
  };

  security.acme.acceptTerms = true;
  security.acme.defaults.email = "admin@brie.gay";
}
