{ config, pkgs, lib, ... }:
let
  # Set to {id}-{branch}-{password} for betas.
  steam-app = "896660";
in {
  imports = [ ./steam.nix ];

  users.users.valheim = {
    isSystemUser = true;
    # Valheim puts save data in the home directory.
    home = "/var/lib/valheim";
    createHome = true;
    homeMode = "750";
    group = "valheim";
    extraGroups = [ "docker" ];
  };

  users.groups.valheim = { };

  virtualisation.oci-containers.containers.valheim = {
    image = "ghcr.io/lloesche/valheim-server";
    ports = [ "2456-2457:2456-2457/udp" ];
    volumes =
      [ "/app/valheim/config:/config" "/app/valheim/data:/opt/valheim" ];
    environment = {
      SERVER_NAME = "brie.gay";
      WORLD_NAME = "Dedicated";
      SERVER_PASS = "nolan";
      SERVER_PUBLIC = "false";
    };
  };

  networking.firewall.allowedUDPPorts = [ 2456 2457 ];
}
