{ pkgs, config, ... }: {
  home-manager.users.brie = {
    home.username = "brie";
    home.stateVersion = config.system.stateVersion;
    home.sessionVariables.EDITOR = "hx";
    home.packages = with pkgs; [ silver-searcher nixfmt ];

    programs = {
      home-manager.enable = true;
      git = {
        enable = true;
        userName = "Brie";
        userEmail = "brie@brie.gay";
        aliases = {
          plog =
            "log --graph --pretty=format:'%h -%d %s -%an %n' --abbrev-commit --date=relative --branches";
          st = "status";
        };
        extraConfig = {
          core = { editor = "hx"; };
          pull.rebase = true;
        };
      };

      zsh = {
        enable = true;
        historySubstringSearch.enable = true;
        oh-my-zsh = {
          enable = true;
          plugins = [ "git" "sudo" ];
        };
      };

      fish = { enable = true; };

      neovim = {
        enable = true;
        extraConfig = ''
          set expandtab
          set autoindent
          filetype plugin indent on

          set mouse=
        '';
        viAlias = true;
        vimAlias = true;
      };

      tmux = {
        enable = true;
        aggressiveResize = true;
        baseIndex = 1;
        keyMode = "vi";
        shortcut = "a";
        terminal = "screen-256color";
        prefix = "C-a";
        escapeTime = 20;
        historyLimit = 20000;

        extraConfig = ''
          # vi resize
          bind -rn M-H resize-pane -L 5
          bind -rn M-J resize-pane -D 5
          bind -rn M-K resize-pane -U 5
          bind -rn M-L resize-pane -R 5

          # navigation
          bind -n M-h select-pane -L
          bind -n M-j select-pane -D
          bind -n M-k select-pane -U
          bind -n M-l select-pane -R


          bind -n M-1 select-window -t :1
          bind -n M-2 select-window -t :2
          bind -n M-3 select-window -t :3
          bind -n M-4 select-window -t :4
          bind -n M-5 select-window -t :5
          bind -n M-6 select-window -t :6
          bind -n M-7 select-window -t :7
          bind -n M-8 select-window -t :8
          bind -n M-9 select-window -t :9
          bind -n M-` command-prompt -p index "select-window -t ':%%'"

          bind -n M-. next-window
          bind -n M-, previous-window
          bind -n M-< swap-window -t -1
          bind -n M-> swap-window -t +1

          bind -n 'M-!' break-pane
          bind -n 'M-c' new-window -c "#{pane_current_path}"
          bind -n 'M--' split-window -c "#{pane_current_path}"
          bind -n 'M-\' split-window -h -c "#{pane_current_path}"

          # Point to sym link for auth
          set-environment -g SSH_AUTH_SOCK $HOME/.ssh/ssh_auth_sock
          set -g update-environment -r

          # scroll to scroll pane history
          set -g mouse on


          set -g status-justify centre
          set -g status on
          set -g status-interval 1
          set -g status-style none,bg=colour17
          set -g message-command-style fg=colour19,bg=colour240

          set -g status-left-length 100
          set -g pane-active-border-style fg=colour02

          set -g pane-border-style fg=colour19
          set -g message-style fg=colour250,bg=colour18

          set -g status-left-style none
          set -g status-right-style none

          setw -g window-status-style none,fg=colour81,bg=colour18
          setw -g window-status-current-style none,fg=colour16,bg=colour63

          setw -g window-status-separator '''
          set -g status-left "#[fg=colour18,bg=colour02] #S #[fg=colour29,bg=colour18] #H"
          set -g status-right "#[fg=colour06,bg=colour18] %a #[fg=colour120,bg=colour19] #F %I:%M:%S "
        '';
      };

      helix = {
        enable = true;

        languages = {
          language = [{
            name = "nix";
            auto-format = true;
            formatter = { command = "nixfmt"; };
          }];
        };

        settings = {
          theme = "nightfox";
          editor = {
            scroll-lines = 4;

            whitespace.render = {
              tab = "all";
              space = "all";
              newline = "none";
            };

            whitespace.characters = {
              tab = "·";
              tabpad = "·";
            };

            indent-guides = {
              render = true;
              character = "┊";
              skip-levels = 1;
            };
          };
        };
      };
    };
  };
}
