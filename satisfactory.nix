{ config, pkgs, lib, ... }: {
  users.users.satisfactory = {
    home = "/var/lib/satisfactory";
    createHome = true;
    isSystemUser = true;
    group = "satisfactory";
  };

  users.groups.satisfactory = { };

  networking = {
    firewall = {
      allowedUDPPorts = [ 15777 15000 7777 27015 ];
      allowedUDPPortRanges = [{
        from = 27031;
        to = 27036;
      }];
      allowedTCPPorts = [ 27015 27036 ];
    };
  };

  systemd.services.satisfactory = {
    wantedBy = [ "multi-user.target" ];
    preStart = ''
      ${pkgs.steamcmd}/bin/steamcmd \
        +login anonymous \
        +force_install_dir /var/lib/satisfactory/SatisfactoryDedicatedServer \
        +app_update 1690800 validate \
        +quit
      ${pkgs.patchelf}/bin/patchelf --set-interpreter ${pkgs.glibc}/lib/ld-linux-x86-64.so.2 /var/lib/satisfactory/SatisfactoryDedicatedServer/Engine/Binaries/Linux/UnrealServer-Linux-Shipping
      ln -sfv /var/lib/satisfactory/.steam/steam/linux64 /var/lib/satisfactory/.steam/sdk64
      mkdir -p /var/lib/satisfactory/SatisfactoryDedicatedServer/FactoryGame/Saved/Config/LinuxServer
      ${pkgs.crudini}/bin/crudini --set /var/lib/satisfactory/SatisfactoryDedicatedServer/FactoryGame/Saved/Config/LinuxServer/Game.ini '/Script/Engine.GameSession' MaxPlayers 10
      ${pkgs.crudini}/bin/crudini --set /var/lib/satisfactory/SatisfactoryDedicatedServer/FactoryGame/Saved/Config/LinuxServer/ServerSettings.ini '/Script/FactoryGame.FGServerSubsystem' mAutoPause True
      ${pkgs.crudini}/bin/crudini --set /var/lib/satisfactory/SatisfactoryDedicatedServer/FactoryGame/Saved/Config/LinuxServer/ServerSettings.ini '/Script/FactoryGame.FGServerSubsystem' mAutoSaveOnDisconnect True
    '';
    script = ''
      /var/lib/satisfactory/SatisfactoryDedicatedServer/Engine/Binaries/Linux/UnrealServer-Linux-Shipping FactoryGame -multihome=0.0.0.0
    '';
    serviceConfig = {
      Restart = "always";
      User = "satisfactory";
      Group = "satisfactory";
      WorkingDirectory = "/var/lib/satisfactory";
    };
    environment = {
      LD_LIBRARY_PATH =
        "SatisfactoryDedicatedServer/linux64:SatisfactoryDedicatedServer/Engine/Binaries/Linux:SatisfactoryDedicatedServer/Engine/Binaries/ThirdParty/PhysX3/Linux/x86_64-unknown-linux-gnu/";
    };
  };
}
