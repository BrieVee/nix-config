{ config, ... }: {
  services._3proxy = {
    enable = true;
    services = [{
      type = "socks";
      auth = [ "iponly" "strong" ];
      acl = [
        {
          rule = "allow";
          sources = [ "138.88.112.175" ];
        }
        {
          rule = "allow";
          users = [ "brie" ];
        }
      ];
    }];
    usersFile = "/etc/3proxy.passwd";
  };

  environment.etc = {
    "3proxy.passwd".text = ''
      brie:CR:$1$KmqXDdtv$XogE6bw7nlp5wTcIq5y9/1
    '';
  };
  networking.firewall.allowedTCPPorts = [ 1080 3128 ];
}
