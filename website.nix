{ config, pkgs, ... }: {
  services.nginx = {
    enable = true;
    virtualHosts = {
      "brie.gay" = {
        enableACME = true;
        forceSSL = true;
        root = "/var/www/";
      };
    };
  };

  networking.firewall.allowedUDPPorts = [ 80 443 ];
  networking.firewall.allowedTCPPorts = [ 80 443 ];
}
