{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-23.05";

    home-manager = {
      url = "github:nix-community/home-manager/release-23.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    helix.url = "github:helix-editor/helix";

    conduit = {
      url = "gitlab:famedly/conduit";

      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, home-manager, helix, conduit, ... }@attrs: {
    nixosConfigurations.gouda = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = attrs;
      modules = [ ./configuration.nix ];
    };

    nixpkgs = import nixpkgs {
      system = "x86_64-linux";
      overlays = [
        (final: prev: {
          helix = helix.packages.${final.system}.default;
          matrix-conduit = conduit.packages.${final.system}.default;
        })
      ];
    };
  };
}
