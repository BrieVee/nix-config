{ pkgs, config, ... }: {
  services.syncthing = {
    enable = true;
    dataDir = "/home/brie/Downloads";
    openDefaultPorts = true;
    configDir = "/home/brie/.config/syncthing";
    user = "brie";
    group = "users";
    extraFlags = [ "--no-default-folder" ];
    devices = {
      "brie-laptop-arch" = {
        id = "LRXIVSC-NJZHOKM-ITPYVAG-5YTDUPB-RKSUHA3-XRIF45T-XHFZ6H7-YHQS6QA";
      };
      "brie-phone-p9" = {
        id = "IVGI6S2-VUQQMNF-K4VVZB5-FLDMAMS-QBVXC2C-AX3ZZUD-GLA5ITC-S36MLQJ";
      };
    };
    folders = {
      "writing" = {
        id = "jesxp-wsuy4";
        path = "/home/brie/writing";
        devices = [ "brie-laptop-arch" "brie-phone-p9" ];
      };
    };
  };
}
