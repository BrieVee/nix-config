{ config, pkgs, lib, ... }: {
  virtualisation.oci-containers.containers.tmodloader = {
    image = "jacobsmile/tmodloader1.4:latest";
    ports = [ "7777:7777" ];
    volumes = [ "/app/tmodloader:/data" ];
    environment = {
      TMOD_MOTD = "Cheese Server";
      TMOD_PASS = "cheese";
      TMOD_WORLDNAME = "Cheeseland";
      TMOD_AUTODOWNLOAD =
        "3018447913,2669644269,2816694149,2824688072,2824688266,2570931073,2908170107,2563309347,2562915378,2614857307,2565639705,2619954303,2785100219,2563862309,2909886416,2827446882,2597324266,3069902935";
      TMOD_ENABLEDMODS =
        "3018447913,2669644269,2816694149,2824688072,2824688266,2570931073,2908170107,2563309347,2562915378,2614857307,2565639705,2619954303,2785100219,2563862309,2909886416,2827446882,2597324266,3069902935";
    };
    extraOptions = [ "--pull=newer" ];
  };

  networking.firewall.allowedTCPPorts = [ 7777 ];
}
