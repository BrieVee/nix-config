{ config, pkgs, lib, ... }: {
  virtualisation.oci-containers.containers.palworld = {
    image = "jammsen/palworld-dedicated-server:latest";
    ports = [ "8211:8211/udp" ];
    volumes = [ "/app/palworld:/palworld" ];
    environment = { SERVER_NAME = "brie.gay palworld server (docker image)"; };
  };

  networking.firewall.allowedUDPPorts = [ 8211 ];
}
