{ config, pkgs, lib, ... }: {
  imports = [ ./containers.nix ];

  users.users.sharkey = {
    isSystemUser = true;
    home = "/app/sharkey/";
    createHome = true;
    homeMode = "750";
    group = "sharkey";
    extraGroups = [ "docker" ];
  };

  users.groups.sharkey = { };

  systemd.services.sharkey = {
    wantedBy = [ "multi-user.target" ];
    path = with pkgs; [ docker ];
    requires = [ "docker.service" ];
    serviceConfig = {
      Restart = "always";
      WorkingDirectory = "/app/sharkey/";
      ExecStart = "${pkgs.docker-compose}/bin/docker-compose up";
      ExecStop = "${pkgs.docker-compose}/bin/docker-compose down";
    };
  };

  services.nginx = {
    enable = true;

    virtualHosts = {
      "do.crimes.brie.gay" = {
        forceSSL = true;
        enableACME = true;

        listen = [
          {
            addr = "0.0.0.0";
            port = 443;
            ssl = true;
          }
          {
            addr = "[::]";
            port = 443;
            ssl = true;
          }
        ];

        locations."/" = {
          proxyPass = "http://127.0.0.1:3000";
          proxyWebsockets = true;
          recommendedProxySettings = true;
        };
      };
    };
  };

  networking.firewall.allowedTCPPorts = [ 3000 ];
}
