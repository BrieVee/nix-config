{ config, pkgs, lib, ... }:
let

  jvmOpts =
    "-XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=40 -XX:G1MaxNewSizePercent=50 -XX:G1HeapRegionSize=16M -XX:G1ReservePercent=15 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=20 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1";

in {
  users.users.minecraft = {
    home = "/var/games/minecraft";
    createHome = true;
    isSystemUser = true;
    group = "minecraft";
  };

  users.groups.minecraft = { };

  networking.firewall.allowedUDPPorts = [ 25565 ];
  networking.firewall.allowedTCPPorts = [ 25565 ];

  systemd.services.meowmeowcraft = {
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      Restart = "always";
      User = "minecraft";
      Group = "minecraft";
      StandardInput = "tty";
      StandardOutput = "journal";
      TTYVHangup = "yes";
      TTYPath = "/dev/tty20";
      TTYReset = "yes";
      WorkingDirectory = "/var/games/minecraft/meowmeowcraft";
      ExecStart =
        "${pkgs.temurin-bin-17}/bin/java -server @user_jvm_args.txt @libraries/net/minecraftforge/forge/1.19.2-43.3.0/unix_args.txt nogui";
    };
  };
}
