{ config, pkgs, lib, ... }: {
  users.users.steam = {
    isSystemUser = true;
    group = "steam";
    home = "/var/lib/steam";
    createHome = true;
  };

  users.groups.steam = { };

  systemd.services."steam@" = {
    unitConfig = { StopWhenUnneeded = true; };
    serviceConfig = {
      Type = "oneshot";
      ExecStart = "${
          pkgs.resholve.writeScript "steam" {
            interpreter = "${pkgs.zsh}/bin/zsh";
            inputs = with pkgs; [ patchelf steamcmd ];
            execer = with pkgs; [ "cannot:${steamcmd}/bin/steamcmd" ];
          }
          "	set -eux\n\n	instance=\${1:?Instance Missing}\n	eval 'args=(\${(@s:_:)instance})'\n	app=\${args[1]:?App ID missing}\n	beta=\${args[2]:-}\n	betapass=\${args[3]:-}\n\n	dir=/var/lib/steam-app-$instance\n\n	cmds=(\n		+force_install_dir $dir\n		+login anonymous\n		+app_update $app validate\n	)\n\n	if [[ $beta ]]; then\n		cmds+=(-beta $beta)\n		if [[ $betapass ]]; then\n			cmds+=(-betapassword $betapass)\n		fi\n	fi\n\n	cmds+=(+quit)\n\n	steamcmd $cmds\n\n	for f in $dir/*; do\n		if ! [[ -f $f && -x $f ]]; then\n			continue\n		fi\n\n		# Update the interpreter to the path on NixOS.\n		patchelf --set-interpreter ${pkgs.glibc}/lib/ld-linux-x86-64.so.2 $f || true\n	done\n"
        } %i";
      PrivateTmp = true;
      Restart = "on-failure";
      StateDirectory = "steam-app-%i";
      TimeoutStartSec = 3600; # Allow time for updates.
      User = "steam";
      WorkingDirectory = "~";
    };
  };
}
