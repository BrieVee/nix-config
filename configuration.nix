# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, home-manager, lib, ... }: {
  imports = [
    ./hardware-configuration.nix
    home-manager.nixosModules.default
    ./home-manager.nix
    ./matrix-conduit.nix
    ./email.nix
    ./proxy.nix
    ./steam.nix
    ./satisfactory.nix
    ./website.nix
    ./meowmeowcraft.nix
    ./syncthing.nix
    ./sharkey.nix
  ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  # Basic networking
  networking.hostName = "gouda";
  networking.networkmanager.enable = true;

  networking.usePredictableInterfaceNames = false;
  networking.interfaces.eth0.ipv4.addresses = [{
    address = "192.95.30.154";
    prefixLength = 24;
  }];

  networking.defaultGateway = "192.95.30.254";
  networking.nameservers = [ "8.8.8.8" ];

  # Set your time zone.
  time.timeZone = "America/New_York";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDJdz4cg4Hun1uj/C9+SOgkQ4pl92E2KFnlgW+LHEsQlY7kVGkRBSnptirLw7KbtceO+23WuTlJoOfWvo5JNeLJ1fI2Z4xt7/aUzmzgnMPb9gDYyhfELDSCtVvyoXzNk1q0DoSH2hT5tm69SVLzKLd4f/pHbFeBprJ8iwNBzvIl7+SKe47lE/Fo+0CrfkNTurcLDxghEUZsx7jjU5nTOiyMraQhJVpy2mFIn+gThMYFp5IT5ay+vJyErxlT0IaqMqN6UqDkysizpD0aW+zrrINiQCWJllPk1mylb7+IuevZfIFHUFQkQTWzuLh7rQjCuzNiqr6oB1q8QWfYvthsYdgD bitofabyte@brie-desktop"
  ];

  users.users = {
    brie = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      shell = pkgs.fish;
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDJdz4cg4Hun1uj/C9+SOgkQ4pl92E2KFnlgW+LHEsQlY7kVGkRBSnptirLw7KbtceO+23WuTlJoOfWvo5JNeLJ1fI2Z4xt7/aUzmzgnMPb9gDYyhfELDSCtVvyoXzNk1q0DoSH2hT5tm69SVLzKLd4f/pHbFeBprJ8iwNBzvIl7+SKe47lE/Fo+0CrfkNTurcLDxghEUZsx7jjU5nTOiyMraQhJVpy2mFIn+gThMYFp5IT5ay+vJyErxlT0IaqMqN6UqDkysizpD0aW+zrrINiQCWJllPk1mylb7+IuevZfIFHUFQkQTWzuLh7rQjCuzNiqr6oB1q8QWfYvthsYdgD bitofabyte@brie-desktop"
      ];
    };
  };

  users.users = {
    erai = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICKI9IyuuwSEl3bk5FmD5dTPzquSRvc1HzP0ZIBB8POh"
      ];
    };

    jacob = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINUjDW3yu94rGAue+hbq+5AVlOh3kTo1sNIYY/Nz8bp3"
      ];
    };

    agatha = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGGYqCcDg9hTINHyf8S56/P83+ZzqwV2t9gUsVYyajjR"
      ];
    };
  };

  # List PAckages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    vim
    wget
    git
    go
    nodejs
    python311
    tcpdump
  ];

  environment.enableAllTerminfo = true;
  environment.variables.COLORTERM = "truecolor";
  environment.variables.EDITOR = "hx";

  nixpkgs.config.allowUnfree = true;

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Enable zsh (for completion??)
  programs.zsh.enable = true;
  programs.fish.enable = true;

  # Enable neovim and use it by default
  programs.neovim.enable = true;

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    banner = ''
      Cheesed to meet you!
    '';
    settings = {
      PasswordAuthentication = false;
      PermitRootLogin = "prohibit-password";
    };
  };

  services.fail2ban.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];

  security.sudo.wheelNeedsPassword = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?

}

